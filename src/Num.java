import java.util.*;

public class Num implements Comparable<Num> {

   /** Instance variable */
   private int num = 0;

   /** Constructor */
   Num (int i) {
      num = i;
   }

   /** Another constructor */
   Num (String s) {
      num = Num.parseNum (s);
   }

   /** Field getter */
   public int numValue() {
      return num;
   }

   /** String representation */
   @Override
   public String toString() {
      return String.valueOf (this.numValue());
   }

   /** Equality test */
   @Override
   public boolean equals (Object o) {
      int ocont = ((Num)o).numValue();
      return this.numValue() == ocont;
   }

   /** Equal objects must have equal hashcodes */
   @Override
   public int hashCode() {
      return this.numValue();  // nice if hashcode depends on all fields
   }

   /** Defined by Comparable */
   public int compareTo (Num o) {
      int ocont = o.numValue();
      if (this.numValue() == ocont)
         return 0;
      else
         if (this.numValue() > ocont)
            return 1;
         else
            return -1;
   }

   /** Copy */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Num (numValue());
   }

   /** Factory */
   public static Num valueOf (int i) {
      return new Num (i);
   }

   /** Another factory */
   public static Num valueOf (String s) {
      return new Num (s);
   }

   /** Conversion from string */
   public static int parseNum (String s) {
      return Integer.parseInt (s);
   }

   /** "Sum" of objects */
   public Num plus (Num a) {
      return new Num (this.numValue() + a.numValue());
   }

   /** Main method */
   public static void main (String[] args) {
      if (args.length > 0) {
         int i = Num.parseNum (args[0]);
         System.out.println (args[0]);
         System.out.println (Num.valueOf (i).toString());
         System.out.println (Num.valueOf (args[0]).toString());
      }
      Num a1 = new Num (2);
      Num a2 = Num.valueOf (-1);
      Num a3 = null;
      try {
         a3 = (Num)a2.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("Must be true:");
      System.out.println (a2.equals (a3));
      System.out.println (a2 != a3);
      System.out.println (a2.compareTo (a3) == 0);
      System.out.println (a2.equals (a2));
      System.out.println (a2.compareTo (a2) == 0);
      System.out.println (a1.compareTo (a2) > 0);
      System.out.println (a2.compareTo (a1) < 0);
      System.out.println (!a1.equals (a2));
      System.out.print (a1 + " + " + a2 + " = ");
      System.out.println (a1.plus (a2));
      ArrayList<Num> l = new ArrayList<Num>();
      for (int i = 0; i < 10; i++) {
         l.add (new Num ((int)(Math.random()*100.)));
      }
      System.out.println (l);
      Collections.sort (l);
      System.out.println (l);
      Comparator<Num> cmp = new Comparator<Num>() {
         @Override
         public int compare (Num n1, Num n2) {
            if (n1.num > n2.num)
               return -1;
            else if (n1.num < n2.num)
               return 1;
            else
               return 0;
         }
      };
      Collections.sort (l, cmp);
      System.out.println (l);
   }

}

